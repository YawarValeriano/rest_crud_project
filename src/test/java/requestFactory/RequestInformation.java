package requestFactory;

public class RequestInformation {
    private String url, body, authorization;
    private boolean withBasicAuth, withToken;

    public RequestInformation() {}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public boolean isWithBasicAuth() {
        return withBasicAuth;
    }

    public void setWithBasicAuth(boolean withBasicAuth) {
        this.withBasicAuth = withBasicAuth;
    }

    public boolean isWithToken() {
        return withToken;
    }

    public void setWithToken(boolean withToken) {
        this.withToken = withToken;
    }
}
