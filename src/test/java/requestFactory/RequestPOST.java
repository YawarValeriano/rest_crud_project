package requestFactory;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;

public class RequestPOST extends Request {

    @Override
    public ResponseInformation send(RequestInformation request) {
        ResponseInformation responseInformation = new ResponseInformation();
        Invocation.Builder builder = this.client.target(request.getUrl()).request();

        if (request.isWithBasicAuth())
            builder.header("Authorization", "Basic " + request.getAuthorization());
        else if (request.isWithToken())
            builder.header("Token", request.getAuthorization());

        Response response = builder.post(Entity.json(request.getBody()));

        responseInformation.setBody(response.readEntity(String.class));
        responseInformation.setCode(response.getStatus());

        response.close();

        return responseInformation;
    }
}
