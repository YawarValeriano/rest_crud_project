package requestFactory;

public class RequestFactory {

    public enum RequestType {
        GET,
        POST,
        PUT,
        DELETE,
    }

    public static Request make(RequestType type) {
        Request request;

        switch (type) {
            case GET:
                request = new RequestGET();
                break;
            case POST:
                request = new RequestPOST();
                break;
            case PUT:
                request = new RequestPUT();
                break;
            case DELETE:
                request = new RequestDELETE();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }
        return request;
    }
}
