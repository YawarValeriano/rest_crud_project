package utils;

public class ConfigAPI {

    public static final String CREATE_USER = ConfigEnv.HOST + "/api/user.json";
    public static final String UPDATE_USER = ConfigEnv.HOST + "/api/user/0.json";

    //to delete user in order to re-run all tests
    public static final String DELETE_USER = ConfigEnv.HOST + "/api/user/0.json";

    public static final String GET_TOKEN = ConfigEnv.HOST + "/api/authentication/token.json";

    public static final String CREATE_PROJECT = ConfigEnv.HOST + "/api/projects.json";
    public static final String READ_PROJECT = ConfigEnv.HOST + "/api/projects/ID.json";
    public static final String UPDATE_PROJECT = ConfigEnv.HOST + "/api/projects/ID.json";
    public static final String DELETE_PROJECT = ConfigEnv.HOST + "/api/projects/ID.json";

}
