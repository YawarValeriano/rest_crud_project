package cleanTest;

import org.json.JSONObject;
import org.junit.jupiter.api.*;
import requestFactory.RequestFactory;
import requestFactory.RequestInformation;
import requestFactory.ResponseInformation;
import utils.ConfigAPI;
import utils.ConfigEnv;

import java.util.Base64;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AllUserTests {

    @Test
    @Order(1)
    void createUserTest() {
        RequestInformation request = new RequestInformation();
        request.setUrl(ConfigAPI.CREATE_USER);

        JSONObject userJson = new JSONObject();
        userJson.put("Email", ConfigEnv.USER_TO_TEST);
        userJson.put("FullName", ConfigEnv.FULL_NAME_TO_TEST);
        userJson.put("Password", ConfigEnv.PASSWORD_TO_TEST);

        request.setBody(userJson.toString());

        ResponseInformation response = RequestFactory.make(RequestFactory.RequestType.POST).send(request);

        System.out.println(response.getBody());
        JSONObject responseBody = new JSONObject(response.getBody());

        Assertions.assertEquals(ConfigEnv.FULL_NAME_TO_TEST, responseBody.getString("FullName"), "No se creó el usuario");
    }

    @Test
    @Order(2)
    void testUpdateUser() {
        final String user = ConfigEnv.USER_TO_TEST + ":" + ConfigEnv.PASSWORD_TO_TEST;

        RequestInformation request = new RequestInformation();

        request.setWithBasicAuth(true);
        request.setUrl(ConfigAPI.GET_TOKEN);
        request.setAuthorization(Base64.getEncoder().encodeToString(user.getBytes()));

        ResponseInformation responseInformation = RequestFactory.make(RequestFactory.RequestType.GET).send(request);
        System.out.println(responseInformation.getBody());

        JSONObject responseBody = new JSONObject(responseInformation.getBody());

        // ************
        final String newFullName = "Updated Full Name2";
        JSONObject userJson = new JSONObject();
        userJson.put("FullName", newFullName);

        request = new RequestInformation();

        request.setWithToken(true);
        request.setUrl(ConfigAPI.UPDATE_USER);
        request.setAuthorization(responseBody.getString("TokenString"));
        request.setBody(userJson.toString());

        responseInformation = RequestFactory.make(RequestFactory.RequestType.PUT).send(request);
        System.out.println(responseInformation.getBody());

        responseBody = new JSONObject(responseInformation.getBody());

        Assertions.assertEquals(newFullName, responseBody.getString("FullName"), "No se actualizó usuario");
    }
}
