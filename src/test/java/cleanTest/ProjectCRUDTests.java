package cleanTest;

import org.json.JSONObject;
import org.junit.jupiter.api.*;
import requestFactory.RequestFactory;
import requestFactory.RequestInformation;
import requestFactory.ResponseInformation;
import utils.ConfigAPI;
import utils.ConfigEnv;

import java.util.Base64;

public class ProjectCRUDTests {
    protected static String token;

    @BeforeAll
    static void beforeAll() {
        final String user = ConfigEnv.USER_TO_TEST + ":" + ConfigEnv.PASSWORD_TO_TEST;

        RequestInformation request = new RequestInformation();

        request.setWithBasicAuth(true);
        request.setUrl(ConfigAPI.GET_TOKEN);
        request.setAuthorization(Base64.getEncoder().encodeToString(user.getBytes()));

        ResponseInformation responseInformation = RequestFactory.make(RequestFactory.RequestType.GET).send(request);
        JSONObject responseBody = new JSONObject(responseInformation.getBody());
        token = responseBody.getString("TokenString");
    }

    @Test
    public void createProtectTest() {
        RequestInformation request = new RequestInformation();
        final String newContent = "Create Project Test";

        JSONObject projectJson = new JSONObject();
        projectJson.put("Content", newContent);
        projectJson.put("Icon", 3);

        request.setWithToken(true);
        request.setAuthorization(token);
        request.setUrl(ConfigAPI.CREATE_PROJECT);
        request.setBody(projectJson.toString());

        ResponseInformation responseInformation = RequestFactory.make(RequestFactory.RequestType.POST).send(request);
        JSONObject responseBody = new JSONObject(responseInformation.getBody());

        Assertions.assertEquals(newContent, responseBody.getString("Content"), "Proyecto no fué creado");
    }

    @Test
    public void readProtectTest() {
        RequestInformation request = new RequestInformation();
        final String newContent = "Read Project Test";

        JSONObject projectJson = new JSONObject();
        projectJson.put("Content", newContent);
        projectJson.put("Icon", 3);

        request.setWithToken(true);
        request.setAuthorization(token);
        request.setUrl(ConfigAPI.CREATE_PROJECT);
        request.setBody(projectJson.toString());

        ResponseInformation responseInformation = RequestFactory.make(RequestFactory.RequestType.POST).send(request);
        JSONObject responseBody = new JSONObject(responseInformation.getBody());
        String idProject = responseBody.get("Id").toString();

        request.setUrl(ConfigAPI.READ_PROJECT.replace("ID", idProject));

        responseInformation = RequestFactory.make(RequestFactory.RequestType.GET).send(request);
        responseBody = new JSONObject(responseInformation.getBody());

        Assertions.assertTrue(responseBody.has("Content"), "Proyecto no se pudo recuperar");
    }

    @Test
    public void updateProtectTest() {
        RequestInformation request = new RequestInformation();
        final String newContent = "Update Project Test";

        JSONObject projectJson = new JSONObject();
        projectJson.put("Content", newContent);
        projectJson.put("Icon", 3);

        request.setWithToken(true);
        request.setAuthorization(token);
        request.setUrl(ConfigAPI.CREATE_PROJECT);
        request.setBody(projectJson.toString());

        ResponseInformation responseInformation = RequestFactory.make(RequestFactory.RequestType.POST).send(request);
        JSONObject responseBody = new JSONObject(responseInformation.getBody());
        String idProject = responseBody.get("Id").toString();

        final int newIcon = 1;

        projectJson = new JSONObject();
        projectJson.put("Icon", newIcon);

        request.setUrl(ConfigAPI.UPDATE_PROJECT.replace("ID", idProject));
        request.setBody(projectJson.toString());

        responseInformation = RequestFactory.make(RequestFactory.RequestType.PUT).send(request);
        responseBody = new JSONObject(responseInformation.getBody());

        Assertions.assertEquals(newIcon, responseBody.getInt("Icon"), "Proyecto no se pudo actualizar");
    }

    @Test
    public void deleteProtectTest() {
        RequestInformation request = new RequestInformation();
        final String newContent = "Delete Project Test";

        JSONObject projectJson = new JSONObject();
        projectJson.put("Content", newContent);
        projectJson.put("Icon", 3);

        request.setWithToken(true);
        request.setAuthorization(token);
        request.setUrl(ConfigAPI.CREATE_PROJECT);
        request.setBody(projectJson.toString());

        ResponseInformation responseInformation = RequestFactory.make(RequestFactory.RequestType.POST).send(request);
        JSONObject responseBody = new JSONObject(responseInformation.getBody());
        String idProject = responseBody.get("Id").toString();

        request.setUrl(ConfigAPI.DELETE_PROJECT.replace("ID", idProject));

        responseInformation = RequestFactory.make(RequestFactory.RequestType.DELETE).send(request);
        responseBody = new JSONObject(responseInformation.getBody());

        Assertions.assertTrue(responseBody.has("Content"), "Proyecto no se pudo eliminar");
    }

    //to delete user in order to re-run all tests
   /* @AfterAll
    static void afterAll() {
        RequestInformation request = new RequestInformation();

        request.setWithToken(true);
        request.setAuthorization(token);
        request.setUrl(ConfigAPI.DELETE_USER);

        RequestFactory.make(RequestFactory.RequestType.DELETE).send(request);
    }*/
}
